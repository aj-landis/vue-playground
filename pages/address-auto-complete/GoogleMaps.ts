const GOOGLE_MAPS_KEY = "AIzaSyC90Oo9BqfriYUsWGGgWRGUdbFP_n3TJh0";

export async function getGeoLocate(): Promise<any> {
  //https://developers.google.com/maps/documentation/geolocation/overview
  const url: string = `https://www.googleapis.com/geolocation/v1/geolocate?key=${GOOGLE_MAPS_KEY}`;
  const response = await fetch(url, { method: "POST" });
  if (response.ok) {
    return await response.json();
  }
  return { failure: true }
}

export async function getGeoData(
  address1: string,
  city: string,
  state: string,
  postalCode: string
): Promise<any> {
  const address = `${address1}, ${city}, ${state}, ${postalCode}`;
  const url: string = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${GOOGLE_MAPS_KEY}`;
  const response = await fetch(url, { method: "POST" });
  if (response.ok) {
    return await response.json();
  }
  return { failure: true }
}

export async function getReverseGeoCoding(
  latitude: number,
  longitude: number
): Promise<any> {
  //https://developers.google.com/maps/documentation/geocoding/requests-reverse-geocoding
  const url: string = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${GOOGLE_MAPS_KEY}`;
  const response = await fetch(url, { method: "GET" });
  if (response.ok) {
    const location = await response.json();
    return { success: true, location };
  }
  return { success: false }
}

export interface AddressComponents {
  address1: string;
  city: string;
  state: string;
  postalCode: string;
  placeId: string;
}

export function parseAddressComponents(
  addressResult: any
) {
  if (!addressResult || !Array.isArray(addressResult.address_components)) {
    return;
  }

  const obj: AddressComponents = {
    address1: "",
    city: "",
    state: "",
    postalCode: "",
    placeId: addressResult.place_id
  }

  let streetNumber = "";
  let streetName = "";
  for (const component of addressResult.address_components) {
    if (Array.isArray(component.types)) {
      if (component.types.includes("street_number")) {
        streetNumber = component.short_name;
      }
      if (component.types.includes("route")) {
        streetName = component.short_name;
      }
      else if (component.types.includes("locality")) {
        obj.city = component.short_name;
      }
      else if (component.types.includes("administrative_area_level_1")) {
        obj.state = component.short_name;
      }
      else if (component.types.includes("postal_code")) {
        obj.postalCode = component.short_name;
      }
    }
  }
  obj.address1 = `${streetNumber} ${streetName}`;

  return obj;

}