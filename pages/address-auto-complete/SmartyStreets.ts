export const SmartyStreetsSDK = require("smartystreets-javascript-sdk");
export const SmartyStreetsCore = SmartyStreetsSDK.core;

const SMARTY_STREETS_HOST = "gobycitizens";
const SMARTY_STREETS_KEY = "108857336073706888";

export function initSmartyStreets(setHost: boolean = false) {
  const smartyStreetsSharedCredentials = new SmartyStreetsCore.SharedCredentials(
    SMARTY_STREETS_KEY,
    setHost ? SMARTY_STREETS_HOST : undefined
  );
  
  const autoCompleteClient = SmartyStreetsCore.buildClient.usAutocompletePro(
    smartyStreetsSharedCredentials
  );
  
  const addressClient = SmartyStreetsCore.buildClient.usStreet(
    smartyStreetsSharedCredentials
  );
  
  const zipCodeClient = SmartyStreetsCore.buildClient.usZipcode(
    smartyStreetsSharedCredentials
  );

  return {
    smartyStreetsSharedCredentials,
    autoCompleteClient,
    addressClient,
    zipCodeClient
  }
}

export const smartyStreetsSharedCredentials = new SmartyStreetsCore.SharedCredentials(
  SMARTY_STREETS_KEY,
  SMARTY_STREETS_HOST
)
export const autoCompleteClient = SmartyStreetsCore.buildClient.usAutocompletePro(
  smartyStreetsSharedCredentials
)

export const addressClient = SmartyStreetsCore.buildClient.usStreet(
  smartyStreetsSharedCredentials
)

export const zipCodeClient = SmartyStreetsCore.buildClient.usZipcode(
  smartyStreetsSharedCredentials
);

export type AddressLookupType = {
  street: string;
  secondary?: string;
  city: string;
  state: string;
  zipcode: string;
}

export type AddressLookupSumbitType = AddressLookupType & {
  maxCandidates: number;
  match: string;
}

export const getAddressLookup: (address: AddressLookupType, initAddressClient: any) => Promise<Array<any>> = async (
  address,
  initAddressClient
) => {
  const lookup: AddressLookupSumbitType = {
    ...address,
    maxCandidates: 1,
    match: "invalid"
  }

  const batch = new SmartyStreetsCore.Batch();
  batch.add(new SmartyStreetsSDK.usStreet.Lookup(lookup));

  const response = await (initAddressClient ?? addressClient).send(batch);
  return response?.lookups?.length > 0 ? response?.lookups : []
}

export type AddressSuggestionType = {
  streetLine: string;
  secondary?: string;
  city: string;
  state: string;
  zipcode: string;
}

export const getAddressSuggestions: (street: string, initAutoCompleteClient: any) => Promise<Array<AddressSuggestionType>> = async (
  street,
  initAutoCompleteClient
) => {
  const lookup = new SmartyStreetsSDK.usAutocompletePro.Lookup(street);
  lookup.maxSuggestions = 10;
  const response = await (initAutoCompleteClient ?? autoCompleteClient).send(lookup);
  return response?.result?.length > 0 ? removeSecondary(response.result) : [];
}

export const getSuggestedAddress: (address: AddressSuggestionType) => Promise<Array<AddressSuggestionType>> = async (
  address
) => {
  const search = `${address.streetLine} ${address.city} ${address.state} ${address.zipcode}`
  const lookup = new SmartyStreetsSDK.usAutocompletePro.Lookup(search);
  lookup.selected = search;
  const response = await autoCompleteClient.send(lookup);
  return response?.result?.length > 0 ? response.result : [];
}

export function removeSecondary(results: Array<AddressSuggestionType>) {
  const newResults: Array<AddressSuggestionType> = [];
  const uniquenessCheck: Array<string> = [];
  for (const address of results) {
    const uniqueAddress = (`${address.streetLine}-${address.city}-${address.state}-${address.zipcode}`).replace(" ", "-");
    if (!uniquenessCheck.includes(uniqueAddress)) {
      newResults.push({
        streetLine: address.streetLine,
        city: address.city,
        state: address.state,
        zipcode: address.zipcode
      });
      uniquenessCheck.push(uniqueAddress);
    }
  }
  return newResults;
}

//https://github.com/smarty/smartystreets-javascript-sdk/blob/master/examples/us_zipcode.js
export async function getZipCodeLookup(
  city: string,
  state: string,
  initZipCodeClient?: any
) {
  const Lookup = SmartyStreetsSDK.usZipcode.Lookup;
  let lookup = new Lookup();
  lookup.city = city;
  lookup.state = state;

  const batch = new SmartyStreetsCore.Batch();
  batch.add(lookup);
  const response = await (initZipCodeClient ?? zipCodeClient).send(batch);
  console.log(response);
}